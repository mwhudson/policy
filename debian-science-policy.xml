<?xml version="1.0"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
  "https://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
<!-- Entities for common use of terms in the document -->
<!ENTITY DebSci "Debian Science">
<!ENTITY DebSciRep '<ulink url="https://salsa.debian.org/science-team">Debian Science Repository</ulink>'>
<!ENTITY Salsa '<ulink url="https://wiki.debian.org/Salsa/Doc">Salsa</ulink>'>
]>
<book>
<bookinfo>
  <title>&DebSci; Policy Manual</title>
  <revhistory>
    <revision>
      <revnumber>1.0</revnumber>
      <date>2012-07-15</date>
      <authorinitials>SL</authorinitials>
      <revremark>DM-Upload should not be enabled by default.
      CDBS is no longer recommended. Debian source package formats 3.0 is now mandatory.</revremark>
    </revision>
    <revision>
        <revnumber>1.1</revnumber>
        <date>2013-09-11</date>
        <authorinitials>PO</authorinitials>
        <revremark>
            Fix the path to VCS-*; Point the DEP5 link to the correct place; Update the git url; Clarify that /git/ is mounted read-only on alioth.debian.org and one should log into git.debian.org instead.; 
            Give the reportbug syntax to put debian-maintainers in CC when filling a new ITP bug; Fix example to activate post-update hook in git; Add a link to gpb in section Package Repositories; Fix FIXME requesting internal links
        </revremark>
    </revision>
    <revision>
        <revnumber>1.2</revnumber>
        <date>2013-10-25</date>
        <authorinitials>PO</authorinitials>
        <revremark>
            Clarify rules regarding debian/changelog
        </revremark>
    </revision>
    <revision>
        <revnumber>1.3</revnumber>
        <date>2018-02-02</date>
        <authorinitials>JC</authorinitials>
        <revremark>
            Work in progress: update to reflect migration from Alioth to Salsa.
        </revremark>
    </revision>
    <revision>
        <revnumber>1.4</revnumber>
        <date>2018-02-13</date>
        <authorinitials>BP</authorinitials>
        <revremark>
            Updates to reflect changes in Debian Policy; fix example of debian/copyright; update Blends URL; update copyright-format URL; update most of http URLs to https. Finish updates related to migration from Alioth to Salsa: rewrite "Mailing Lists", "The Debian Science Repository", "Repository Layout and Structure", "Repository Creation" and "Publishing Your Repository" sections, update "Intend To Package (ITP)" section, delete "Commit Messages" section.
        </revremark>
    </revision>

  </revhistory>
</bookinfo>
<preface>
  <title>Introduction</title>
  <sect1>
    <title>About &DebSci;</title>
    <para>The main goal of the &DebSci; project is to provide a system with all the most important free scientific software in each scientific field.</para>
    <para>&DebSci; will also manage the debian-science's metapackages.</para>
  </sect1>
  <sect1>
    <title>How to Contribute</title>
    <para>A <ulink url="https://blends.debian.org/science/tasks/">list of packages</ulink> has been created using Debian Pure Blend. This website contains a long list of software packages in Debian and yet to be packaged. You can help by packaging them in coordination with the &DebSci;.</para>
    <para>In the meantime, you can also help us by updating the debian-science's metapackages.</para>
  </sect1>
  <sect1>
    <title>Becoming a Member</title>
    <para>Before becoming a member you need to have on account on Salsa. If you do not have an account already, you can <ulink url="https://salsa.debian.org/users/sign_in">register</ulink> one.</para>
    <para>To request membership, login to Salsa and use the <ulink url="https://salsa.debian.org/groups/science-team/-/group_members/request_access">"Request to join"</ulink> link on our <ulink url="https://salsa.debian.org/science-team/">&DebSci; Salsa page</ulink>. Your application will be processed quickly.</para>
  </sect1>
</preface>
<chapter>
  <title>Packaging Policy</title>
  <para>This Packaging Policy describes the packaging rules that packages maintained by the &DebSci; Project should follow. The aim is to define a common set of rules to make maintenance easier for everyone involved in the project.</para>
  <para>It is not normative as the Debian Policy. If rules or suggestions in this document conflict with the Debian Policy it is a bug in this document.</para>
  <sect1>
    <title>Debian Control Files</title>
    <para>The following sections describe the changes to the control files stored in the source package's <filename>debian/</filename> directory.</para>
    <sect2>
      <title><filename>debian/changelog</filename></title>
      <para>Changes in the Debian version of the package should be briefly explained in the Debian changelog file.
            The <filename>debian/changelog</filename> file consists in a series of entries detailing the changes. The format of the entries is described
            in the <ulink url="https://www.debian.org/doc/debian-policy/ch-source.html#s-dpkgchangelog">Debian Policy</ulink>.
      </para>
      <para>Not yet uploaded packages should have only a single changelog entry closing the <link linkend="itp-rfp-reports">ITP bug</link>. Indeed, when a package is submitted only
          the latest paragraph is parsed by the Debian bug tracking system. Therefore do not add new changelog entries when updating an unrealeased
          package, the VCS logs are enough to track the changes.
      </para>
      <para>When a sponsor is uploading a package for you, mark the package <varname>target</varname> as UNRELEASED. The sponsor will change it
            as appropriate before uploading the package.
      </para>
    </sect2>
    <sect2>
      <title><filename>debian/control</filename></title>
      <sect3>
	<title><varname>Section</varname> and <filename>Priority</filename> Fields</title>
	<para>The <varname>Section</varname> field of the source package should be set to "science", unless a more specific section such as "math" is applicable.</para>
	<para>The <varname>Priority</varname> field should be set to "optional".</para>
      </sect3>
      <sect3>
	<title><varname>Maintainer</varname> and <varname>Uploaders</varname> Fields</title>
	<para>The <varname>Maintainer</varname> field should be "Debian Science Maintainers <email>debian-science-maintainers@lists.alioth.debian.org</email>".</para>
	<para>You should also add yourself to the <varname>Uploaders</varname> field. Doing so shows your involvement and interest in the package. Developers listed in <varname>Uploaders</varname> will take care of maintenance, bug reports and other QA work, helped by the &DebSci; team.</para>
      </sect3>
      <sect3>
	<title><varname>Vcs-Git</varname> and <varname>Vcs-Browser</varname> Fields</title>
	<para>If you have your package under version control in the &DebSci; Repository, you must set the <varname>Vcs-Git</varname> and <varname>Vcs-Browser</varname> fields.</para>
    <para>The <varname>Vcs-Git</varname> field should contain the package repository URL, namely
"https://salsa.debian.org/science-team/foo.git".</para>
    <para>The <varname>Vcs-Browser</varname> field should point to the
web view of your package repository, namely "https://salsa.debian.org/science-team/foo".</para>
      </sect3>
      <sect3>
	<title><varname>Homepage</varname> Field</title>
	<para>If an upstream homepage exists, the <varname>Homepage</varname> field should be set to the upstream homepage URL.</para>
      </sect3>
      <sect3>
	<title>Config::Model</title>
	<para>It is a very good idea to use Config::Model to unify the formatting of <filename>debian/control</filename>. To do so make sure you have installed
	<screen>apt-get install cme libconfig-model-dpkg-perl libconfig-model-itself-perl</screen>
	and than you can simply call
	<screen>cme fix dpkg-control</screen>
	to get a properly formatted, sanity checked <filename>debian/control</filename> file. Please note that sometimes you need to call this more than once.</para>
      </sect3>
    </sect2>
    <sect2>
      <title><filename>debian/copyright</filename></title>
      <para>If possible, the <ulink url="https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/">machine-readable format</ulink> should be used.</para>
      <para>The license of the packaging work should either match the license of the upstream sources or have a license that is even more free than the upstream license. For example, you should avoid licensing your packaging work under GPL if the upstream license is BSD, even though they are compatible.</para>
    </sect2>
    <sect2>
      <title><filename>debian/watch</filename></title>
      <para>If possible, a <filename>debian/watch</filename> file should be added so upstream releases can be tracked automatically.</para>
    </sect2>
    <sect2>
      <title>Examples</title>
      <para>In this section you will find example <filename>debian/control</filename> and <filename>debian/copyright</filename> files that you can use as a template and modify as needed.</para>
      <example>
	<title>Example <filename>debian/control</filename> for package "foo"</title>
<screen>
Source: foo
Section: science
Priority: optional
Homepage: https://foo.example.org/
Maintainer: Debian Science Maintainers &lt;debian-science-maintainers@lists.alioth.debian.org&gt;
Uploaders: Your Name &lt;your.name@debian.org&gt;
Vcs-Git: https://salsa.debian.org/science-team/foo.git
Vcs-Browser: https://salsa.debian.org/science-team/foo
</screen>
      </example>
      <example>
	<title>Example <filename>debian/copyright</filename> for package "foo" under GPL</title>
<screen>
Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SOFTware
Upstream-Contact: John Doe &lt;john.doe@example.com&gt;
Source: http://www.example.com/software/project

Files: *
Author: John Doe &lt;john.doe@example.org&gt;
Copyright: Copyright &copy; 2007-2008 John Doe &lt;john.doe@example.org&gt;
License: GPL-3+

Files: debian/*
Author: Your Name &lt;your.name@debian.org&gt;
Copyright: Copyright &copy; 2008 Your Name &lt;your.name@debian.org&gt;
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with the Debian GNU/Linux distribution in /etc/share/common-licenses/GPL.
 If not, see &lt;https://www.gnu.org/licenses/&gt;.
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in /usr/share/common-licenses/GPL-3.
</screen>
      </example>
    </sect2>
  </sect1>

  <sect1>
    <title>Version Control</title>
    <para>The &DebSci; Project has agreed to use <ulink url="https://git-scm.com/">Git</ulink> as their preferred Version Control System (VCS).</para>
    <para>Because of Git's nature, each package exists in its own repository. The term "repository" is also used for the location where all packages are stored. It is used in the following sections as defined below:</para>
    <para><emphasis>Git Repository</emphasis> means a (bare) Git repository, containing history information.</para>
    <para><emphasis>Package Repository</emphasis> means a Git Repository that stores packaging information.</para>
    <para><emphasis>&DebSci; Repository</emphasis> means the location where all package repositories are stored.</para>
    <sect2>
      <title>The &DebSci; Repository</title>
      <para>The &DebSciRep; is located on &Salsa;.</para>
      <para>All <ulink url="https://salsa.debian.org/groups/science-team/-/group_members">group members</ulink> have write access to all Package Repositories, but there is some difference in access levels:
      <itemizedlist mark="bullet">
        <listitem><para>all <ulink url="https://wiki.debian.org/DebianDeveloper">Debian Developers</ulink> have at least <varname>Maintainer</varname> level of access</para></listitem>
        <listitem><para>other members (including Debian Maintainers) have a <varname>Developer</varname> level of access</para></listitem>
      </itemizedlist>
      See <ulink url="https://docs.gitlab.com/ee/user/permissions.html#group-members-permissions">GitLab documentation</ulink> for details.</para>

    </sect2>
    <sect2 id='package-repositories'>

      <title>Package Repositories</title>
      <para>The following sections explain the preferred Package Repository layout.</para>
      <sect3>
	<title>Repository Layout and Structure</title>
        <para>Each Package Repository has to be stored as a bare Git Repository in &DebSciRep; (<varname>group</varname> in terms of GitLab) or in one of its thematic sub-directories (<varname>sub-groups</varname> in terms of GitLab).</para>
      </sect3>
      <sect3>
        <title>Repository Creation</title>
        <para>Each group member with <varname>Maintainer</varname> level of access (or higher) may create new Package Repositories in <varname>group</varname> using standard GitLab features (Web UI and Web API). Another way is to create personal Package Repository on Salsa and then transfer it to &DebSciRep;. Do not forget to check a visibility of transferred repository (public or private). Only group members with <varname>Owner</varname> level of access may change the visibility of Package Repository.</para>
        <para>Group members with <varname>Developer</varname> level of access may ask for Package Repository creation from group members with higher level of access directly or in team mailing list. Transferring of personal Package Repositories is not accessible for them. If such transfer is absolutely necessary, the access level of group member may be temporary changed to <varname>Maintainer</varname>.</para>
      </sect3>
      <sect3>
	<title>Branches</title>
	<para>All Package Repositories should contain a common set of branches in order to ease maintenance.</para>
	<para>All Debian-specific changes (such as the <filename>debian/</filename> directory) should go to a branch named <emphasis>debian</emphasis> or <emphasis>master</emphasis>. If you use several branches to organize your changes, the <emphasis>debian</emphasis> branch should be treated as the integration branch.</para>
	<para>If upstream sources are included in the Package Repository they should be stored in a branch named <emphasis>upstream</emphasis>. It can either store upstream sources from a VCS or source tar-ball snapshots.</para>
	<para>It is recommended to use <ulink url="https://packages.debian.org/pristine-tar">pristine-tar</ulink> to be able to restore upstream tar-balls from the source files in the Package Repository. The delta files produced by pristine-tar should be stored in the <emphasis>pristine-tar</emphasis> branch.</para>
  <para><ulink url="https://honk.sigxcpu.org/projects/git-buildpackage/manual-html/gbp.html">Git-buildpackage</ulink> follows the above branch conventions and automates the process of importing upstream snapshots.</para>
      </sect3>
      <sect3>
	<title>Tags</title>
	<para>All imports of source tar-balls or upstream releases (if tracked in the Package Repository) should be tagged as "upstream/${VERSION}". For example, the 1.2.3 release of a package should have a tag "upstream/1.2.3".</para>
	<para>Accordingly, all Debian releases of a package should be tagged "debian/${DEB-VERSION}". For example, the second Debian release of the example package in the last paragraph should have a tag "debian/1.2.3-2".</para>
      </sect3>
    </sect2>
  </sect1>
  <sect1>
    <title>Build Systems</title>
    <para>Package maintainers are free to use any build system for packaging (or none at all).</para>
    <para><ulink url="https://packages.debian.org/debhelper">Debhelper</ulink> is recommend for more complicated packaging.</para>
  </sect1>
  <sect1>
    <title>Patch Systems</title>
    <para><ulink url="https://packages.debian.org/quilt">quilt</ulink> and <ulink url="https://wiki.debian.org/Projects/DebSrc3.0">Debian Source Format 3</ulink> must be.</para>
    <para>If a package is stored in a Package Repository a patch system is not needed technically but the maintainer is free to use one nevertheless.</para>
  </sect1>
	<sect1>
	<title>Metapackages</title>
	<para>&DebSci; is also managing the Debian-Science metapackages. Consequently, please update the appropriate metapackage with your new package as described in <link linkend="management-of-the-debian-science-metapackages">Chapter 2</link>.</para>
	</sect1>
  <sect1 id="itp-rfp-reports">
    <title>ITP/RFP reports</title>
	  <sect2>
	    <title>Intend To Package (ITP)</title>

		    <para>When a Debian Science Maintainer is sending an ITP to the BTS, <email>debian-science@lists.debian.org</email> should be one of the receivers of this ITP. Therefore, members of the team will be informed of the on-going work and could provide help.
         If using <ulink url="https://packages.debian.org/reportbug">reportbug</ulink> to file the ITP, this can be accomplished with the following command:</para>
<screen><![CDATA[
reportbug --list-cc=debian-science@lists.debian.org wnpp
]]></screen>

		    <para>Also please include the pseudo-headers:
	<screen><![CDATA[
User: debian-science@lists.debian.org
Usertags: field..THE_FIELD_OF_YOUR_PACKAGE
]]></screen>
</para>
	</sect2>
	<sect2>
		<title>Request For Packaging (RFP)</title>
		<para>If a RFP of a scientific software is sent to the BTS, this one could be assigned to the team and added to the Debian Science Blend package list.</para>
	</sect2>
  </sect1>
</chapter>
<chapter id='management-of-the-debian-science-metapackages'>
  <title>Management of the Debian-Science metapackages</title>
	  <para>&DebSci; is also in charge of the Debian-Science metapackages. These packages provide an easy way for a lambda user to get packages in a specific scientific fields.</para>
	<sect1><title>Science related metapackages</title>
	<para>The science related metapackages are:
	<itemizedlist mark="bullet">
		<listitem><para>science-astronomy   packages related to astronomy</para></listitem>
		<listitem><para>science-biology   packages related to biology</para></listitem>
		<listitem><para>science-biology     packages related to biology</para></listitem>
		<listitem><para>science-chemistry   packages related to chemistry</para></listitem>
		<listitem><para>science-electronics packages related to electronics</para></listitem>
        	<listitem><para>science-geography   packages related to geography</para></listitem>
		<listitem><para>science-linguistics packages related to linguistics</para></listitem>
		<listitem><para>science-mathematics packages related to mathematics</para></listitem>
		<listitem><para>science-physics     packages related to physics</para></listitem>
		<listitem><para>science-robotics    packages related to robotics</para></listitem>
	</itemizedlist>

	</para>
	</sect1>
	<sect1><title>Common utilities for all sciences metapackages</title>

	<para>Common utilities for all sciences metapackages are:
	<itemizedlist mark="bullet">
  <listitem><para>science-statistics  packages related to statistics</para></listitem>
  <listitem><para>science-typesetting packages related to typesetting</para></listitem>
  <listitem><para>science-viewing     packages related to viewing</para></listitem>
	</itemizedlist>
</para>
	</sect1>
	<sect1>
	<title>How to manage this list</title>
	<sect2>
		<title>Technically</title>
		<para>These files are stored in the <ulink url="
https://salsa.debian.org/blends-team/science">Debian Science Blend's VCS</ulink>.</para>
	<para>Checkout the source tree and edit /tasks/* files. These files contain the list of the packages which will be added to the metapackages.</para>
	<para>Note that it is also possible to add packages which are not in the Debian archive. These packages will appear on the <ulink url="https://blends.debian.org/science/tasks/">Blend task list</ulink>.</para>
	</sect2>
	</sect1>
</chapter>
<chapter>
<title>Communication vectors</title>
  <sect1>
    <title>Mailing Lists</title>
    <para>This section briefly explains the purpose of the mailing lists that are under the scope of &DebSci;.</para>
    <sect2>
      <title>User List</title>
      <para>The <email>debian-science@lists.debian.org</email> mailing list is used for communication between package maintainers, software developers and other people involved in packaging of software.</para>
    </sect2>
    <sect2>
      <title>Maintainer List</title>
      <para>The <email>debian-science-maintainers@lists.alioth.debian.org</email> mailing list is used for automatically generated messages, such as BTS and Debian queue daemon messages.</para>
    </sect2>
  </sect1>
  <sect1>
	<title>Wiki</title>
	<para><ulink url="https://wiki.debian.org/DebianScience/">https://wiki.debian.org/DebianScience/</ulink></para>
	</sect1>
  <sect1>
    <title>Maintaining of this Policy</title>
     <para>This &DebSci; Policy is maintained on <ulink url="https://salsa.debian.org/science-team/policy">salsa.debian.org/science-team/policy</ulink>. Any member of the &DebSci; is able to make proposal about updating of this document. Open discussion is needed if the critical parts of the Policy are affected.</para>
  </sect1>
</chapter>
<appendix>
  <title>Help on Packaging</title>
  <para>The following sections are not part of the &DebSci; Policy Manual. They should provide useful information for contributors who are not familiar with the tools mentioned in this document.</para>
  <sect1>
    <title>Packaging with Git</title>
    <para>This section will give you guide on packaging Debian packages with Git. Since Git is a very flexible tool, you can (and should!) adopt it to fit your work-flow best. "There is more than one way to do it", as the saying goes, so this document can not show all of them. It's supposed to help you if you are unfamiliar with using Git for packaging work. It is, however, not supposed to be a normative document that everyone has to follow.</para>

    <sect2>
      <title>Creating a Repository</title>
      <para>Git repositories are nothing more than a directory with a special directory storing meta-data, named <filename>.git/</filename>. Creating and configuring the repository is pretty straight-forward:</para>
      <screen><![CDATA[
$ mkdir foo && cd foo
$ git init
Initialized empty Git repository in .git/
$ git config user.name  "$DEBFULLNAME"
$ git config user.email "$DEBEMAIL"
]]></screen>
      <para>This creates a new Git directory and sets the user name and email address that will be used in the commit message.</para>
    </sect2>
    <sect2>
      <title>Importing Upstream Sources</title>
      <para>It is quite common for packaging with Git that the upstream sources are stored in the repository. It is, however, not strictly necessary. But the costs are rather low and the benefit can be high, so this section will explain ways to import the upstream sources into you repository.</para>
      <sect3>
	<title>Using git-buildpackage and pristine-tar</title>
	<para>There are two tools that will assist you in packaging with Git a lot: git-buildpackage and pristine-tar.</para>
	<para>git-buildpackage can import upstream sources, create or import <filename>.diff.gz</filename>s and build a Debian package from Git repositories. It is similar in function to svn-buildpackage or other VCS-buildpackage tools.</para>
	<para>pristine-tar is able to recreate bit-identical upstream tar-balls from the files stored under version control. It does so by creating small delta files, so that it is not necessary to store the whole tar-ball.</para>
	<para>To import an upstream tar-ball into your repository, simply run:</para>
	<screen><![CDATA[
$ gbp import-orig --pristine-tar ~/foo_1.0.orig.tar.gz
Upstream version is 1.0
Initial import of '/home/jdoe/foo_1.0.orig.tar.gz' ...
pristine-tar: committed foo_1.0.orig.tar.gz.delta to branch pristine-tar
Succesfully merged version 1.0 of /home/jdoe/foo_1.0.orig.tar.gz into .
]]></screen>
	<para>After that, you will see two new branches, "upstream" and "pristine-tar":</para>
	<screen><![CDATA[
$ git branch
* master
  pristine-tar
  upstream
]]></screen>
	<para>The upstream branch holds your upstream sources as extracted from the tar-ball. The pristine-tar branch holds the delta files the pristine-tar created. Also, a tag has been created:</para>
	<screen><![CDATA[
$ git tag -l
upstream/1.0
]]></screen>
	<para>You can use <command>gbp import-orig</command> for every new upstream tar-ball. It will extract the sources to the upstream branch and merge them back to the master branch.</para>
      </sect3>
    </sect2>
    <sect2>
      <title>Changes To The Upstream Sources</title>
      <para>There are at least two methods for doing changes to upstream sources. One uses an integration branch, the other one uses a patch system. Both methods have it's benefits and drawbacks, so you have to decide which methods is best for your purpose.</para>
      <sect3>
	<title>Using An Integration Branch</title>
	<para>By using this model, all changes to the upstream sources are made in independent branches and merged back into a special branch, the "integration" branch. This section tries to explain how this works.</para>
	<para>When doing changes to the upstream sources, all logical changes should be stored in their own branch, a so-called "feature branch". The hardest part is giving it a reasonable name. If you want to fix a bug submitted to the BTS, you might want to call your branch "bug-nnn", where "nnn" is the bug number. You can also prefix Debian-specific changes with "deb/" to separate them from branches that contain patches which might be included upstream.</para>
	<note><para>You can also store each patch as one commit in a single feature branch, such as "upstream-patches". It is all up to you. Figure out what work-flow you like best.</para></note>
	<para>After you thought of a name, you should branch from the latest upstream version. If you used <command>gbp import-orig</command>, you can use the tags created by it:</para>
	<screen><![CDATA[
$ git checkout -b bug-123456 upstream/1.0
]]></screen>
	<para>You can now start fixing the bug inside this branch and commit the changes. For every change to the upstream sources, create a new branch and do your fixes in that until you're done. In the last step, it is time to merge the changes back to the integration branch.</para>
      </sect3>
    </sect2>
    <sect2>
      <title>Publishing Your Repository</title>
      <para>Instructions how to create Package Repository on &Salsa; are described above.</para>
      <para>The last steps need to be done on your <emphasis>local</emphasis> repository:</para>
      <screen><![CDATA[
$ git remote add origin git@salsa.debian.org:science-team/foo.git
git push --all origin
git push --tags origin
]]></screen>
      <para>This tells you local repository about your remote repository and exports all of your branches and tags.</para>
      <para>All these steps only need to be done once. After that, pushing your changes to the remote repository is all you need to do to publish them.</para>
    </sect2>
  </sect1>
</appendix>
</book>
